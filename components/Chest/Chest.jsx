import './Chest.scss'

const Chest = ({className}) => {

  const spotsCount = 6;
  let spots = [];

  for (let i = 0; i<spotsCount; i++) {
    spots.push(<div key={i} className={`chest__color-spot  chest__color-spot--${i+1} `}></div>);
  }

  return (
    <div className={`chest ${className}`}>
      <div className="chest__inner">

        {spots}

      </div>
    </div>
  );
};

export default Chest;
