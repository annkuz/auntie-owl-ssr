import './Ear.scss'


const Ear = ({className}) => {
  return (
    <div className={`ear ${className}`}>
      <div className="ear__inner"></div>
    </div>
  );
};

export default Ear;