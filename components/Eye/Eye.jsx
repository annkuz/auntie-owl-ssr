import './Eye.scss'

const Eye = ({className}) => {

  return (
    <div className={`eye ${className}`} tabIndex="0">
      <div className="eye__pupil"></div>
    </div>
  );
};

export default Eye;
