import './Leg.scss'

const Leg = ({className}) => {
  return (
    <div className={`leg ${className}`}>
      <div className="leg__inner"></div>
    </div>
  );
};

export default Leg;
