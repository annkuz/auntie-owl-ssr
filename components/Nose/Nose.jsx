import './Nose.scss'


const Nose = ({className}) => {
  return (
    <div className={`nose ${className}`}>
      <div className="nose__inner"></div>
    </div>
  );
};

export default Nose;
