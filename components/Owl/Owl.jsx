import { Component } from 'react';
import './Owl.scss';
import OwlHead from '../OwlHead/OwlHead'
import OwlBody from '../OwlBody/OwlBody'

class Owl extends Component {
  render() {
    return (
      <div className="owl">
        <OwlHead className="owl__head"/>
        <OwlBody className="owl__body"/>
      </div>
    );
  }
}

export default Owl;
