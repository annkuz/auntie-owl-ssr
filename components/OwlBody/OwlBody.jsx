import './OwlBody.scss'
import Wing from '../Wing/Wing'
import Chest from '../Chest/Chest'
import Leg from '../Leg/Leg'

const OwlBody = ({className}) => {

  return (
    <div className={`body ${className}`} tabIndex="0">
      <Wing className={`body__wing body__wing--right wing--right`} />
      <Wing className={`body__wing body__wing--left wing--left`} />
      <Chest className={`body__chest`} />
      <Leg className={`body__leg body__leg--right leg--right`} />
      <Leg className={`body__leg body__leg--left leg--left`} />
    </div>
  );
};

export default OwlBody;
