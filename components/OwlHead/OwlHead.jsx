import './OwlHead.scss'
import Eye from '../Eye/Eye'
import Nose from '../Nose/Nose'
import Ear from '../Ear/Ear'

const OwlHead = ({className}) => {

  return (
    <div className={`head ${className}`}>
      <Eye className={`head__eye head__eye--left`}/>
      <Eye className={`head__eye head__eye--right`}/>
      <Nose className={`head__nose`}/>
      <Ear className={`head__ear ear--left`} />
      <Ear className={`head__ear head__ear--right ear--right`} />
    </div>
  );
};

export default OwlHead;
